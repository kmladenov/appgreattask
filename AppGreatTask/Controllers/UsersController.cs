﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppGreat.Common.Exceptions;
using AppGreat.Models;
using AppGreat.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppGreat.Web.Controllers
{
    [Route("api")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            this._userService = userService;
        }

        // POST: api/login
        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginRequest request)
        {
            try
            {
                var response = _userService.Login(request).Result;
                return Ok(response);
            }
            catch (AggregateException ex)
            {
                return Unauthorized($"{ex.InnerException.GetType().Name}: {ex.InnerException.Message}");
            }
        }
        // POST: api/register
        [HttpPost("register")]
        public IActionResult Register([FromBody] RegisterRequest request)
        {
            try
            {
                var model = _userService.Register(request).Result;
                return Ok(model);
            }
            catch (AggregateException ex)
            {
                return BadRequest($"{ex.InnerException.GetType().Name}: {ex.InnerException.Message}");
            }
        }
    }
}
