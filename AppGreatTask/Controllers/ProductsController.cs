﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppGreat.Common.Exceptions;
using AppGreat.Common.Helpers;
using AppGreat.Data.Entities;
using AppGreat.Models;
using AppGreat.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppGreat.Web.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            this._productService = productService;
        }
        // GET: api/products
        [HttpGet]
        public IActionResult Get()
        {
            var user = (User)HttpContext.Items["User"];
            var products = _productService.GetAllProducts().Result;
            if (user != null)
            {
                foreach (var product in products)
                {
                    product.Price *= CurrencyConversion.ExchangeRate(user.Currency_Code.ToString());
                }
            }
            return Ok(products);
        }

        // GET: api/products/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            try
            {
                var user = (User)HttpContext.Items["User"];
                var product = _productService.GetProduct(id).Result;
                if (user != null)
                {
                    product.Price *= CurrencyConversion.ExchangeRate(user.Currency_Code.ToString());
                }
                return Ok(product);
            }
            catch (AggregateException ex)
            {
                return NotFound($"{ex.InnerException.GetType().Name}: {ex.InnerException.Message}");
            }
        }

        // POST: api/products/create
        [Authorize]
        [HttpPost("create")]
        public IActionResult Post([FromBody] ProductModel model)
        {
            try
            {
                var product = _productService.CreateProduct(model).Result;
                return Created("api/products", product);
            }
            catch (AggregateException ex)
            {
                return BadRequest($"{ex.InnerException.GetType().Name}: {ex.InnerException.Message}");
            }
        }

        // DELETE: api/products/5
        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _productService.DeleteProduct(id);
            return NoContent();
        }
    }
}
