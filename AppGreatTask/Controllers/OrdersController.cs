﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using AppGreat.Common.Exceptions;
using AppGreat.Common.Helpers;
using AppGreat.Data.Entities;
using AppGreat.Models;
using AppGreat.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppGreat.Web.Controllers
{
    [Route("api/orders")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrdersController(IOrderService orderService)
        {
            this._orderService = orderService;
        }
        // POST: api/orders/create
        [Authorize]
        [HttpPost("create")]
        public IActionResult CreateOrder([FromBody] string products)
        {
            try
            {
                var user = (User)HttpContext.Items["User"];
                var order = _orderService.CreateOrder(user.Id, products).Result;
                order.Total_Price *= CurrencyConversion.ExchangeRate(user.Currency_Code.ToString());
                return Ok(order);
            }
            catch (AggregateException ex)
            {
                return NotFound($"{ex.InnerException.GetType().Name}: {ex.InnerException.Message}");
            }
        }
        // GET: api/orders/my
        [Authorize]
        [HttpGet("my")]
        public IActionResult GetMyOrders()
        {
            var user = (User)HttpContext.Items["User"];
            var orders = _orderService.GetUserOrders(user.Id).Result;
            foreach(var order in orders)
            {
                order.Total_Price *= CurrencyConversion.ExchangeRate(user.Currency_Code.ToString());
            }
            return Ok(orders);
        }
        // PUT: api/orders/5
        [Authorize]
        [HttpPut("{id}")]
        public IActionResult UpdateStatus(int id, [FromBody] string status)
        {
            try
            {
                var userId = ((User)HttpContext.Items["User"]).Id;
                var order = _orderService.ChangeOrderStatus(userId, id, status).Result;
                return Ok(order);
            }
            catch (AggregateException ex) when (ex.InnerException is OrderNotFoundException)
            {
                return NotFound($"{ex.InnerException.GetType().Name}: {ex.InnerException.Message}");
            }
            catch (AggregateException ex)
            {
                return BadRequest($"{ex.InnerException.GetType().Name}: {ex.InnerException.Message}");
            }
        }
    }
}
