﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public int User_Id { get; set; }
        public double Total_Price { get; set; }
        public string Status { get; set; }
        public DateTime Created_At { get; set; }
        public List<ProductModel> Products { get; set; }
    }
}
