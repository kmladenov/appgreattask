﻿using AppGreat.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Models
{
    public class LoginResponse
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string CurrencyCode { get; set; }
        public string Token { get; set; }


        public LoginResponse(User user, string token)
        {
            Id = user.Id;
            Username = user.Username;
            CurrencyCode = user.Currency_Code.ToString();
            Token = token;
        }
    }
}
