﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Common.Exceptions
{
    public class DuplicateUserException : Exception
    {
        public DuplicateUserException()
            : base("User already exists.")
        {

        }
        public DuplicateUserException(string username)
            : base($"User {username} already exists.")
        {

        }
    }
}
