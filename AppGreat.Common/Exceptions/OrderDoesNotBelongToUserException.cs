﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Common.Exceptions
{
    public class OrderDoesNotBelongToUserException : Exception
    {
        public OrderDoesNotBelongToUserException()
            : base("The selected order does not belong to your user.")
        { 
        }
    }
}
