﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Common.Exceptions
{
    public class InvalidCredentialsException : Exception
    {
        public InvalidCredentialsException()
            : base("Incorrect username or password.")
        {
        }
    }
}
