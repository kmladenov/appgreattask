﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Common.Exceptions
{
    public class NegativePriceException : Exception
    {
        public NegativePriceException()
            : base("Product price cannot be negative.")
        {

        }
    }
}
