﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Common.Exceptions
{
    public class ProductNotFoundException : Exception
    {
        public ProductNotFoundException()
            : base("Product not found.")
        {
        }
        public ProductNotFoundException(int productId)
            : base($"Product with ID {productId} not found.")
        {
        }
        public ProductNotFoundException(string productName)
            : base($"Product with name {productName} not found.")
        {
        }
    }
}
