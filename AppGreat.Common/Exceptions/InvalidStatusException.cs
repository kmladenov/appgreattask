﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Common.Exceptions
{
    public class InvalidStatusException : Exception
    {
        public InvalidStatusException()
            : base("Invalid order status.\nAvailable status: New, Payment, Delivery, Cancelled, Completed.")
        {

        }
        public InvalidStatusException(string status)
            : base($"Order status {status} is not valid.\nAvailable status: New, Payment, Delivery, Cancelled, Completed.")
        {
            
        }
    }
}
