﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Common.Exceptions
{
    public class OrderNotFoundException : Exception
    {
        public OrderNotFoundException()
            : base("Order not found.")
        {

        }
        public OrderNotFoundException(int orderId)
            : base($"Order with ID {orderId} not found.")
        {

        }
    }
}
