﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Common.Exceptions
{
    public class InvalidCurrencyCodeException : Exception
    {
        public InvalidCurrencyCodeException()
            : base("Invalid Currency Code.\nFor available codes, please refer to https://en.wikipedia.org/wiki/ISO_4217#Active_codes")
        {
        }

        public InvalidCurrencyCodeException(string code)
        : base($"{code} is not a valid Currency Code.\nFor available codes, please refer to https://en.wikipedia.org/wiki/ISO_4217#Active_codes")
        {
        }
    }
}
