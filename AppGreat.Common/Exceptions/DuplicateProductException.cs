﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Common.Exceptions
{
    public class DuplicateProductException : Exception
    {
        public DuplicateProductException()
            : base("Product already exists.")
        {
        }
        public DuplicateProductException(string productName)
            : base($"Product {productName} already exists.")
        {
        }
    }
}
