﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace AppGreat.Common.Helpers
{
    public static class CurrencyConversion
    {
        public static double ExchangeRate(string currencyCode)
        {
            using var client = new HttpClient();
            string connectionString = $"https://api.exchangeratesapi.io/latest?base=BGN&symbols={currencyCode}";
            var result = client.GetStringAsync(connectionString).Result;
            var resultAsArray = result.Split('{', ':', '}');
            double rate = double.Parse(resultAsArray[4]);
            return rate;
        }
}
}
