﻿using AppGreat.Common.Exceptions;
using AppGreat.Data;
using AppGreat.Data.Entities;
using AppGreat.Models;
using AppGreat.Services.Contracts;
using AppGreat.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppGreat.Services
{
    public class OrderService : IOrderService
    {
        private readonly AppGreatContext _context;
        public OrderService(AppGreatContext context)
        {
            this._context = context ?? throw new ArgumentNullException("Context cannot be null.");
        }
        public async Task<OrderModel> CreateOrder(int userId, string productsString)
        {
            var productNames = productsString.Split(',').Select(s => s.Trim());
            var products = new List<Product>();
            foreach (var productName in productNames)
            {
                var product = await _context.Products.FirstOrDefaultAsync(p => p.Name.ToLower() == productName.ToLower());
                if (product == null)
                {
                    throw new ProductNotFoundException(productName);
                }
                products.Add(product);
            }
            var order = new Order
            {
                Created_At = DateTime.UtcNow,
                Status = Status.New,
                User_Id = userId,
            };
            await _context.Orders.AddAsync(order);
            await _context.SaveChangesAsync();
            foreach (var product in products)
            {
                order.OrderProducts.Add(new OrderProduct
                {
                    OrderId = order.Id,
                    ProductId = product.Id,
                });
            }
            order.Total_Price = products.Sum(p => p.Price);
            _context.Users.FirstOrDefaultAsync(u => u.Id == userId).Result.Orders.Add(order);
            await _context.SaveChangesAsync();
            var orderModel = OrderMapper.MapModelFromEntity(order);
            return orderModel;
        }
        public async Task<OrderModel> ChangeOrderStatus(int userId, int orderId, string status)
        {
            var order = await _context.Orders
                .Include(o => o.OrderProducts)
                .ThenInclude(op => op.Product)
                .FirstOrDefaultAsync(o => o.Id == orderId);
                
            if (order == null)
            {
                throw new OrderNotFoundException();
            }
            if (order.User_Id != userId)
            {
                throw new OrderDoesNotBelongToUserException();
            }
            try
            {
                order.Status = (Status)Enum.Parse(typeof(Status), status, true);
                await _context.SaveChangesAsync();
            }
            catch (ArgumentException)
            {
                throw new InvalidStatusException(status);
            }
            var orderModel = OrderMapper.MapModelFromEntity(order);
            return orderModel;
        }

        public async Task<List<OrderModel>> GetUserOrders(int userId)
        {
            var orderModels = await _context.Orders
                .Include(o => o.OrderProducts)
                .ThenInclude(op => op.Product)
                .Where(o => o.User_Id == userId)
                .Select(o => OrderMapper.MapModelFromEntity(o)).ToListAsync();
            return orderModels;
        }
    }
}
