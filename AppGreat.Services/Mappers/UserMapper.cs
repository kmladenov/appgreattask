﻿using AppGreat.Data.Entities;
using AppGreat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppGreat.Services.Mappers
{
    internal static class UserMapper
    {
        internal static UserModel MapModelFromEntity(User user)
        {
            return new UserModel
            {
                Username = user.Username,
                Currency_Code = user.Currency_Code.ToString(),
            };
        }
    }
}
