﻿using AppGreat.Data.Entities;
using AppGreat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppGreat.Services.Mappers
{
    internal static class OrderMapper
    {
        internal static OrderModel MapModelFromEntity(Order order)
        {
            return new OrderModel
            {
                Id = order.Id,
                Created_At = order.Created_At,
                User_Id = order.User_Id,
                Products = order.OrderProducts.Select(op => ProductMapper.MapModelFromEntity(op.Product)).ToList(),
                Status = order.Status.ToString(),
                Total_Price = order.Total_Price,
            };
        }
    }
}
