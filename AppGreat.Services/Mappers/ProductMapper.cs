﻿using AppGreat.Data.Entities;
using AppGreat.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Services.Mappers
{
    internal static class ProductMapper
    {
        internal static Product MapEntityFromModel(ProductModel model)
        {
            return new Product
            {
                Name = model.Name,
                ImageUrl = model.ImageUrl,
                Price = model.Price,
            };
        }

        internal static ProductModel MapModelFromEntity(Product product)
        {
            return new ProductModel
            {
                Name = product.Name,
                ImageUrl = product.ImageUrl,
                Price = product.Price,
                Id = product.Id,
            };
        }
    }
}
