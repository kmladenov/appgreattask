﻿using AppGreat.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppGreat.Services.Contracts
{
    public interface IOrderService
    {
        Task<OrderModel> ChangeOrderStatus(int userId, int orderId, string status);
        Task<OrderModel> CreateOrder(int userId, string productsString);
        Task<List<OrderModel>> GetUserOrders(int userId);
    }
}