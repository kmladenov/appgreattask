﻿using AppGreat.Data.Entities;
using AppGreat.Models;
using System.Threading.Tasks;

namespace AppGreat.Services.Contracts
{
    public interface IUserService
    {
        Task<LoginResponse> Login(LoginRequest model);
        Task<UserModel> Register(RegisterRequest model);
        User GetById(int userId);
    }
}