﻿using AppGreat.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppGreat.Services.Contracts
{
    public interface IProductService
    {
        Task<ProductModel> CreateProduct(ProductModel model);
        Task DeleteProduct(int id);
        Task<List<ProductModel>> GetAllProducts();
        Task<ProductModel> GetProduct(int id);
    }
}