﻿using AppGreat.Common.Exceptions;
using AppGreat.Common.Helpers;
using AppGreat.Data;
using AppGreat.Data.Entities;
using AppGreat.Models;
using AppGreat.Services.Contracts;
using AppGreat.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AppGreat.Services
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly AppGreatContext _context;

        public UserService(IOptions<AppSettings> appSettings, AppGreatContext context)
        {
            _appSettings = appSettings.Value;
            this._context = context ?? throw new ArgumentNullException("Context can NOT be null.");
        }

        public async Task<UserModel> Register(RegisterRequest model)
        {
            var userExists = await _context.Users.FirstOrDefaultAsync(u => u.Username == model.Username);
            if (userExists != null)
            {
                throw new DuplicateUserException(model.Username);
            }
            try
            {
                var user = new User
                {
                    Username = model.Username,
                    Password = model.Password,
                    Currency_Code = (CurrencyCode)Enum.Parse(typeof(CurrencyCode), model.CurrencyCode, true)
                };
                 _context.Users.Add(user);
                 _context.SaveChanges();
                var userModel = UserMapper.MapModelFromEntity(user);
                return userModel;
            }
            catch (ArgumentException)
            {
                throw new InvalidCurrencyCodeException(model.CurrencyCode);
            }
        }

        public async Task<LoginResponse> Login(LoginRequest model)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == model.Username && u.Password == model.Password);

            // Throw if user not found
            if (user == null)
            {
                throw new InvalidCredentialsException();
            }

            // authentication successful so generate jwt token
            var token = GenerateJwtToken(user);

            return new LoginResponse(user, token);
        }

        public User GetById(int id)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == id);
            return user;
        }

        private string GenerateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
