﻿using AppGreat.Common.Exceptions;
using AppGreat.Data;
using AppGreat.Data.Entities;
using AppGreat.Models;
using AppGreat.Services.Contracts;
using AppGreat.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppGreat.Services
{
    public class ProductService : IProductService
    {
        private readonly AppGreatContext _context;

        public ProductService(AppGreatContext context)
        {
            this._context = context ?? throw new ArgumentNullException("Context cannot be null.");
        }

        public async Task<ProductModel> CreateProduct(ProductModel model)
        {
            if (model.Price < 0)
            {
                throw new NegativePriceException();
            }
            model.Name = model.Name.Trim();
            var productExists = await _context.Products.FirstOrDefaultAsync(p => p.Name.ToLower() == model.Name.ToLower());
            if (productExists != null)
            {
                throw new DuplicateProductException(productExists.Name);
            }
            var product = ProductMapper.MapEntityFromModel(model);
            _context.Products.Add(product);
            await _context.SaveChangesAsync();
            model = ProductMapper.MapModelFromEntity(product);
            return model;
        }

        public async Task<List<ProductModel>> GetAllProducts()
        {
            var productModels = await _context.Products
                .Where(p => p.DeletedOn == null)
                .Select(p => ProductMapper.MapModelFromEntity(p)).ToListAsync();
            return productModels;
        }

        public async Task<ProductModel> GetProduct(int id)
        {
            var product = await _context.Products.FirstOrDefaultAsync(p => p.Id == id && p.DeletedOn == null);
            ValidateProductExists(product);
            var productModel = ProductMapper.MapModelFromEntity(product);
            return productModel;
        }

        public async Task DeleteProduct(int id)
        {
            var product = await _context.Products.FirstOrDefaultAsync(p => p.Id == id);
            product.DeletedOn = DateTime.UtcNow;
            _context.SaveChanges();
        }

        private void ValidateProductExists(Product product)
        {
            if (product == null)
            {
                throw new ProductNotFoundException();
            }
        }
    }
}
