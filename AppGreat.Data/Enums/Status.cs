﻿public enum Status
{
    New,
    Payment,
    Delivery,
    Cancelled,
    Completed
}