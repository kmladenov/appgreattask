﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Data.Entities
{
    public class Order
    {
        public Order()
        {
            Created_At = DateTime.UtcNow;
            OrderProducts = new List<OrderProduct>();
        }
        public int Id { get; set; }
        public User User { get; set; }
        public int User_Id { get; set; }
        public List<OrderProduct> OrderProducts { get; set; }
        public double Total_Price { get; set; }
        public Status Status { get; set; }
        public DateTime Created_At { get; set; }
    }
}
