﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace AppGreat.Data.Entities
{
    public class User
    {
        public User()
        {
            Orders = new List<Order>();
        }
        public int Id { get; set; }
        public string Username { get; set; }
        [JsonIgnore]
        public string Password { get; set; }
        public CurrencyCode Currency_Code { get; set; }
        public List<Order> Orders { get; set; }
    }
}
