﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppGreat.Data.Entities
{
    public class Product
    {
        public Product()
        {
            OrderProducts = new List<OrderProduct>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string ImageUrl { get; set; }
        public List<OrderProduct> OrderProducts { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}
